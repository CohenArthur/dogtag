#pragma once;

#ifndef CURL_FUNCTIONS_H
#define CURL_FUNCTIONS_H

#include <string>

#define BUFFER_SIZE 4096
#define SMOL_BUFFER 256

#define USER		0
#define MATCH		1
#define SEASONS		2
#define USER_SEASON 3
#define LIFETIME	4

/*
This struct is used to store information regarding the request :

	struct MemoryStruct new_memory;

	new_data.size	returns the size of the string.
	new_data.data	returns the string.
*/
typedef struct CURL_data {
	char		*data;
	size_t		size;
	std::string filename;
} CURL_data;

typedef struct CURL_request {
	std::string name;
	std::string id;
	std::string last_match_id;
	std::string current_season_id;
} CURL_request;

size_t writeMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);

CURL_data initC_d();
void cleanDataC_d(CURL_data data);

CURL_data performRequest(int verbose, int type, CURL_request request);

#endif // !CURL_FUNCTIONS_H