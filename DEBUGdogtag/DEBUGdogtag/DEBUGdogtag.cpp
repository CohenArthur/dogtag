//DEBUG_INCLUDE

#define _CRT_SECURE_NO_WARNINGS
#define _CRTDBG_MAP_ALLOC //Includes regarding debuggin and Memory mapping
#include <stdlib.h>
#include <crtdbg.h> //Call '_CrtDumpMemoryLeaks()' just before the last 'return 0' for a list of leaks.

//END_DEBUG_INCLUDE

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream> //Easier to open files. I guess.

#include "curl/curl.h" //libcURL : Does all our networking stuff. Documentation here : https://curl.haxx.se/libcurl/c/
#include "nlohmann/json.hpp" //Our JSON parser and godsend. Repo here : https://github.com/nlohmann/json

#include "cURL_functions.h"
#include "user.h"
#include "last_match.h"
#include "season.h"

#define BUFFER_SIZE 4096 //This way we can just do char my_string[BUFFER_SIZE] instead of typing '4096' all the time. Plus it changes it for all strings if we need more space.
#define SMOL_BUFFER 256

#define VERBOSE_1 1
#define VERBOSE_0 0

#define CURRENT 0
#define LIFE 1

using namespace std;
using json = nlohmann::json; //Just so we can type 'json.DoStuff()' instead of 'nlohmann::json.DoStuff()'.
using namespace DogtagCore;

/*
Here's what a request should look like :
curl -g "https://api.pubg.com/shards/pc/players?filter[playerNames]=Kagounard" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkMzliYjQwMC0xYzQwLTAxMzctYmJlYi0wN2Q3ZGU1ZTNiZDEiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTUxMjE4ODQ3LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImppbmtvYXBwIn0.Sdg9NAis_g8dkLyqgNwDiksMvgYQT9tynrMEXPFKEw4" -H "Accept: application/vnd.api+json"
*/

int main(void)
{
	///////////////////DO NOT REMOVE//////////////////////
	curl_global_init(CURL_GLOBAL_ALL);////////////////////
	///////////////////DO NOT REMOVE//////////////////////

	User Mikumama("Kagounard");

	Mikumama.setUserData(VERBOSE_0);

	LastMatch MikuLM(Mikumama.getLastMatchId(), Mikumama.getName());

	MikuLM.setLastMatchInfo(VERBOSE_0);

	MikuLM.setAllData();

	MikuLM.displayAllData();

	//Season MikuLife(Mikumama, CURRENT);

	//MikuLife.setUserSeasonData(VERBOSE_0, CURRENT);

	//MikuLife.displayAllStats();

	///////////////////DO NOT REMOVE//////////////////////
	curl_global_cleanup();////////////////////////////////
	///////////////////DO NOT REMOVE//////////////////////

	return 0;
}
