#pragma once

#include <string>

#include <nlohmann/json.hpp>

#include "user.h"
#include "cURL_functions.h"

namespace DogtagCore {
	class Season
	{
	public:
		Season();
		Season(std::string id, int timeline);
		~Season();

		void		setHasUser(bool has_user);
		bool		getCurrent();
		bool		getLifetime();
		bool		getHasUser();
		void		setAllSeasonsData(int verbose);
		void		setUserSeasonData(int verbose, int timeline);
		void		displayAllStats();

	private:
		CURL_data		s_all_curl_data;
		CURL_data		s_user_curl_data;
		CURL_request	s_user_request;
		nlohmann::json	s_all_json;
		nlohmann::json	s_user_json;
		bool			s_is_current;
		bool			s_is_lifetime;
		bool			s_has_user;

	public:
		//SEASON_STATS
		////duo_TPP
		std::string		s_duo_tpp_kills;
		std::string		s_duo_tpp_dBNOs;
		std::string		s_duo_tpp_assists;
		std::string		s_duo_tpp_heals;
		std::string		s_duo_tpp_boosts;
		std::string		s_duo_tpp_revives;
		std::string		s_duo_tpp_longestKill;
		std::string		s_duo_tpp_roundMostKills;
		std::string		s_duo_tpp_teamKills;
		std::string		s_duo_tpp_maxKillStreaks;
		std::string		s_duo_tpp_weaponsAcquired;
		std::string		s_duo_tpp_wins;
		std::string		s_duo_tpp_top10s;
		std::string		s_duo_tpp_dailyKills;
		std::string		s_duo_tpp_weeklyKills;
		std::string		s_duo_tpp_dailyWins;
		std::string		s_duo_tpp_weeklyWins;
		std::string		s_duo_tpp_damageDealt;
		std::string		s_duo_tpp_headshotKills;
		////duo_FPP
		std::string		s_duo_fpp_kills;
		std::string		s_duo_fpp_dBNOs;
		std::string		s_duo_fpp_assists;
		std::string		s_duo_fpp_heals;
		std::string		s_duo_fpp_boosts;
		std::string		s_duo_fpp_revives;
		std::string		s_duo_fpp_longestKill;
		std::string		s_duo_fpp_roundMostKills;
		std::string		s_duo_fpp_teamKills;
		std::string		s_duo_fpp_maxKillStreaks;
		std::string		s_duo_fpp_weaponsAcquired;
		std::string		s_duo_fpp_wins;
		std::string		s_duo_fpp_top10s;
		std::string		s_duo_fpp_dailyKills;
		std::string		s_duo_fpp_weeklyKills;
		std::string		s_duo_fpp_dailyWins;
		std::string		s_duo_fpp_weeklyWins;
		std::string		s_duo_fpp_damageDealt;
		std::string		s_duo_fpp_headshotKills;
		////solo_TPP
		std::string		s_solo_tpp_kills;
		std::string		s_solo_tpp_dBNOs;
		std::string		s_solo_tpp_assists;
		std::string		s_solo_tpp_heals;
		std::string		s_solo_tpp_boosts;
		std::string		s_solo_tpp_revives;
		std::string		s_solo_tpp_longestKill;
		std::string		s_solo_tpp_roundMostKills;
		std::string		s_solo_tpp_teamKills;
		std::string		s_solo_tpp_maxKillStreaks;
		std::string		s_solo_tpp_weaponsAcquired;
		std::string		s_solo_tpp_wins;
		std::string		s_solo_tpp_top10s;
		std::string		s_solo_tpp_dailyKills;
		std::string		s_solo_tpp_weeklyKills;
		std::string		s_solo_tpp_dailyWins;
		std::string		s_solo_tpp_weeklyWins;
		std::string		s_solo_tpp_damageDealt;
		std::string		s_solo_tpp_headshotKills;
		////solo_FPP
		std::string		s_solo_fpp_kills;
		std::string		s_solo_fpp_dBNOs;
		std::string		s_solo_fpp_assists;
		std::string		s_solo_fpp_heals;
		std::string		s_solo_fpp_boosts;
		std::string		s_solo_fpp_revives;
		std::string		s_solo_fpp_longestKill;
		std::string		s_solo_fpp_roundMostKills;
		std::string		s_solo_fpp_teamKills;
		std::string		s_solo_fpp_maxKillStreaks;
		std::string		s_solo_fpp_weaponsAcquired;
		std::string		s_solo_fpp_wins;
		std::string		s_solo_fpp_top10s;
		std::string		s_solo_fpp_dailyKills;
		std::string		s_solo_fpp_weeklyKills;
		std::string		s_solo_fpp_dailyWins;
		std::string		s_solo_fpp_weeklyWins;
		std::string		s_solo_fpp_damageDealt;
		std::string		s_solo_fpp_headshotKills;
		////squad_TPP
		std::string		s_squad_tpp_kills;
		std::string		s_squad_tpp_dBNOs;
		std::string		s_squad_tpp_assists;
		std::string		s_squad_tpp_heals;
		std::string		s_squad_tpp_boosts;
		std::string		s_squad_tpp_revives;
		std::string		s_squad_tpp_longestKill;
		std::string		s_squad_tpp_roundMostKills;
		std::string		s_squad_tpp_teamKills;
		std::string		s_squad_tpp_maxKillStreaks;
		std::string		s_squad_tpp_weaponsAcquired;
		std::string		s_squad_tpp_wins;
		std::string		s_squad_tpp_top10s;
		std::string		s_squad_tpp_dailyKills;
		std::string		s_squad_tpp_weeklyKills;
		std::string		s_squad_tpp_dailyWins;
		std::string		s_squad_tpp_weeklyWins;
		std::string		s_squad_tpp_damageDealt;
		std::string		s_squad_tpp_headshotKills;
		////squad_FPP
		std::string		s_squad_fpp_kills;
		std::string		s_squad_fpp_dBNOs;
		std::string		s_squad_fpp_assists;
		std::string		s_squad_fpp_heals;
		std::string		s_squad_fpp_boosts;
		std::string		s_squad_fpp_revives;
		std::string		s_squad_fpp_longestKill;
		std::string		s_squad_fpp_roundMostKills;
		std::string		s_squad_fpp_teamKills;
		std::string		s_squad_fpp_maxKillStreaks;
		std::string		s_squad_fpp_weaponsAcquired;
		std::string		s_squad_fpp_wins;
		std::string		s_squad_fpp_top10s;
		std::string		s_squad_fpp_dailyKills;
		std::string		s_squad_fpp_weeklyKills;
		std::string		s_squad_fpp_dailyWins;
		std::string		s_squad_fpp_weeklyWins;
		std::string		s_squad_fpp_damageDealt;
		std::string		s_squad_fpp_headshotKills;
		//SEASON_STATS
	};
}