#include "weapon.h"

#include <string>
#include <iostream>
#include <fstream>

#include "nlohmann/json.hpp"

using namespace std;
using json = nlohmann::json;

namespace DogtagCore {
	Weapon::Weapon(string weapon_name)
	{
		w_name = weapon_name;

		ifstream database("weapons_db.json");
		json w_json = json::parse(database);

		cout << w_json["/rifles/aug/ammo"_json_pointer].dump();

		//w_hit_damage = w_json[strcat(w_name, "/damage\"_json_pointer")].dump(); etc...
	}

	string Weapon::getName() { return w_name; }
	string Weapon::getHitDamage() { return w_hit_damage; }
	string Weapon::getBulletSpeed() { return w_bullet_speed; }
	string Weapon::getDPS() { return w_dps; }
	string Weapon::getMagSize() { return w_mag_size; }
	string Weapon::getTimeBetweenShots() { return w_time_per_shot; }
	string Weapon::getAmmoType() { return w_ammo; }

	Weapon::~Weapon()
	{
	}
}