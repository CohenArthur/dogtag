#include <string>
#include <iostream>
#include <fstream>

#include <nlohmann/json.hpp>

#include "user.h"
#include "cURL_functions.h"

using namespace std;
using json = nlohmann::json;

namespace DogtagCore {
	User::User()
	{
		u_filename = "../user.dt";
		u_last_match_id = "\0";

		std::ifstream user_file(u_filename);
		std::string user_str((std::istreambuf_iterator<char>(user_file)),
			std::istreambuf_iterator<char>());

		if (user_str == "\0")
		{
			u_name = "Unknown";
			u_id = "-1";
		}
		else
			parseUserInfo();

		u_has_json = false;
	}

	User::User(string name)
	{
		u_name = name;
		u_filename = "../user.dt";
		u_last_match_id = "\0";
		u_has_json = false;
	}

	User::~User()
	{
	}

	string User::getName() const
	{
		return u_name;
	}

	string User::getId() const
	{
		return u_id;
	}

	void User::parseUserInfo()
	{
		string name;
		string id;
		string::size_type id_start = 0;

		std::ifstream user_file(u_filename);

		std::string user_str((std::istreambuf_iterator<char>(user_file)),
			std::istreambuf_iterator<char>());

		for (string::size_type index = 0; index < user_str.size(); index++)
		{
			if (user_str[index] != '\n')
				name.push_back(user_str[index]);
			if (user_str[index] == '\n')
			{
				id_start = index + 1;
				index = user_str.size();
			}
		}

		for (string::size_type id_index = id_start; id_index < user_str.size(); id_index++)
		{
			if (user_str[id_index] != EOF)
				id.push_back(user_str[id_index]);
		}

		u_name = name;
		u_id = id;
	}

	void User::writeUserInfo()
	{
		std::ofstream user_file("../user.dt");

		user_file << u_name << "\n" << u_id;
	}

	void User::setUserData(int verbose)
	{
		CURL_request request;
		request.name = u_name;
		request.id = u_id;

		CURL_data data = performRequest(verbose, USER, request);

		std::ifstream json_req(data.filename);
		json new_json = json::parse(json_req);

		u_curl_data = data;
		u_json = new_json;
		u_last_match_id = u_json["/data"_json_pointer][0]["/relationships/matches/data"_json_pointer][0]["/id"_json_pointer].get<string>();
		u_current_season_id = CURRENT_SEASON_ID;
		u_has_json = true;
		u_id = u_json["/data"_json_pointer][0]["/id"_json_pointer].get<string>();
	}

	CURL_data User::getUserData()
	{
		return u_curl_data;
	}

	json User::getJson()
	{
		return u_json;
	}

	string User::getLastMatchId()
	{
		return u_last_match_id;
	}

	string User::getCurrentSeasonId()
	{
		return u_current_season_id;
	}

	bool User::getHasJson()
	{
		return u_has_json;
	}
}