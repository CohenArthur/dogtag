#include "muzzle.h"

#include <iostream>
#include <fstream>

#include "nlohmann/json.hpp"

using namespace std;
using json = nlohmann::json;

namespace DogtagCore {
	Muzzle::Muzzle(std::string muzzle_name)
	{
		m_name = muzzle_name;

		ifstream database("weapons_db.json");
		json w_json = json::parse(database);

		//w_hit_damage = w_json[strcat(w_name, "/damage\"_json_pointer")].dump(); etc...
	}

	string Muzzle::getName() { return m_name; }
	string Muzzle::getHorizontalRecoil() { return m_hori_recoil; }
	string Muzzle::getVerticalRecoil() { return m_vert_recoil; }
	string Muzzle::getRecovery() { return m_recovery; }
	string Muzzle::getSway() { return m_sway; }
	string Muzzle::getKick() { return m_kick; }
	string Muzzle::getAdsSpeed() { return m_ads_speed; }

	Muzzle::~Muzzle()
	{
	}
}