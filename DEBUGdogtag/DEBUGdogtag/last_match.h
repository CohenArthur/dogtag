#pragma once

#include "user.h"

#include <string>

namespace DogtagCore {
	class LastMatch
	{
	public:
		LastMatch(std::string last_match_id, std::string user_name);
		~LastMatch();

		void			setLastMatchInfo(int verbose);
		nlohmann::json	getJson();
		CURL_data		getLastMatchData();
		void 			setAllData();
		void 			displayAllData();

	private:
		std::string		lm_id;
		std::string		lm_name;
		std::string		lm_type;
		CURL_data		lm_curl_data;
		nlohmann::json	lm_json;

	public:
		std::string 	lm_rank;
		std::string 	lm_kills;
		std::string		lm_dbnos;
		std::string		lm_assists;
		std::string 	lm_killRank;
		std::string 	lm_damageDealt;
		std::string 	lm_headshotKills;
		std::string 	lm_heals;
		std::string 	lm_boosts;
		std::string 	lm_longestKill;
		std::string 	lm_timeSurvived;
		std::string 	lm_revives;
		std::string 	lm_rideDistance;
		std::string 	lm_roadKills;
		std::string 	lm_teamKills;
		std::string 	lm_vehicleDestroys;
		std::string 	lm_walkDistance;
		std::string 	lm_weaponsAcquired;
	};
}