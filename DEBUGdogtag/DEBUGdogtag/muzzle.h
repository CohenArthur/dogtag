#pragma once

#include <string>

namespace DogtagCore {
	class Muzzle
	{
	public:
		Muzzle(std::string muzzle_name);
		~Muzzle();

		std::string getName();
		std::string getHorizontalRecoil();
		std::string getVerticalRecoil();
		std::string getRecovery();
		std::string getSway();
		std::string getKick();
		std::string getAdsSpeed();

	private:
		std::string m_name;
		std::string m_hori_recoil;
		std::string m_vert_recoil;
		std::string m_recovery;
		std::string m_sway;
		std::string m_kick;
		std::string m_ads_speed;
	};
}
