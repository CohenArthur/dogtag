#include "grip.h"

#include <fstream>

#include <nlohmann/json.hpp>

using namespace std;
using json = nlohmann::json;

namespace DogtagCore {
	Grip::Grip(string grip_name)
	{
		g_name = grip_name;

		ifstream database("weapons_db.json");
		json w_json = json::parse(database);

		//w_hit_damage = w_json[strcat(w_name, "/damage\"_json_pointer")].dump(); etc...
	}

	string Grip::getName() { return g_name; }
	string Grip::getRecoilPattern() { return g_recoil_pattern; }
	string Grip::getHorizontalRecoil() { return g_hori_recoil; }
	string Grip::getVerticalRecoil() { return g_vert_recoil; }
	string Grip::getRecovery() { return g_recovery; }
	string Grip::getPelletSpread() { return g_pellet_spread; }
	string Grip::getDeviation() { return g_deviation; }

	Grip::~Grip()
	{
	}
}