#include "season.h"

#include <iostream>
#include <fstream>
#include <iomanip>

#include <nlohmann/json.hpp>

#include "cURL_functions.h"

using namespace std;
using json = nlohmann::json;

#define CURRENT 0
#define LIFE 1

namespace DogtagCore {
	Season::Season()
	{
	}

	Season::Season(string id, int timeline)
	{
		if (timeline == CURRENT)
		{
			s_user_request.current_season_id = CURRENT_SEASON_ID;
			s_user_request.id = id;
			s_is_current = true;
			s_is_lifetime = false;
		}
		else
		{
			s_user_request.current_season_id = CURRENT_SEASON_ID;
			s_user_request.id = id;
			s_is_current = false;
			s_is_lifetime = true;
		}

	}

	Season::~Season()
	{
	}

	void Season::setHasUser(bool has_user)
	{
		s_has_user = has_user;
	}

	bool Season::getCurrent()
	{
		return s_is_current;
	}

	bool Season::getLifetime()
	{
		return s_is_lifetime;
	}

	bool Season::getHasUser()
	{
		return s_has_user;
	}

	void Season::setAllSeasonsData(int verbose)
	{
		CURL_request request;
		CURL_data data = performRequest(verbose, SEASONS, request); //Passing an empty request

		std::ifstream json_req(data.filename);
		json new_json = json::parse(json_req);

		s_all_curl_data = data;
		s_all_json = new_json;
	}

	void Season::setUserSeasonData(int verbose, int timeline)
	{

		CURL_data data;

		if (timeline == CURRENT)
			data = performRequest(verbose, USER_SEASON, s_user_request);
		else
			if (timeline == LIFE)
				data = performRequest(verbose, LIFETIME, s_user_request);

		std::ifstream json_req(data.filename);
		json json = json::parse(json_req);

		s_user_curl_data = data;
		s_user_json = json;

		////duo_TPP
		s_duo_tpp_kills = json["/data/attributes/gameModeStats/duo/kills"_json_pointer].dump();
		s_duo_tpp_dBNOs = json["/data/attributes/gameModeStats/duo/dBNOs"_json_pointer].dump();
		s_duo_tpp_assists = json["/data/attributes/gameModeStats/duo/assists"_json_pointer].dump();
		s_duo_tpp_heals = json["/data/attributes/gameModeStats/duo/heals"_json_pointer].dump();
		s_duo_tpp_boosts = json["/data/attributes/gameModeStats/duo/boosts"_json_pointer].dump();
		s_duo_tpp_revives = json["/data/attributes/gameModeStats/duo/revives"_json_pointer].dump();
		s_duo_tpp_longestKill = json["/data/attributes/gameModeStats/duo/longestKill"_json_pointer].dump();
		s_duo_tpp_roundMostKills = json["/data/attributes/gameModeStats/duo/roundMostKills"_json_pointer].dump();
		s_duo_tpp_teamKills = json["/data/attributes/gameModeStats/duo/teamKills"_json_pointer].dump();
		s_duo_tpp_maxKillStreaks = json["/data/attributes/gameModeStats/duo/maxKillStreaks"_json_pointer].dump();
		s_duo_tpp_weaponsAcquired = json["/data/attributes/gameModeStats/duo/weaponsAcquired"_json_pointer].dump();
		s_duo_tpp_wins = json["/data/attributes/gameModeStats/duo/wins"_json_pointer].dump();
		s_duo_tpp_top10s = json["/data/attributes/gameModeStats/duo/top10s"_json_pointer].dump();
		s_duo_tpp_dailyKills = json["/data/attributes/gameModeStats/duo/dailyKills"_json_pointer].dump();
		s_duo_tpp_weeklyKills = json["/data/attributes/gameModeStats/duo/weeklyKills"_json_pointer].dump();
		s_duo_tpp_dailyWins = json["/data/attributes/gameModeStats/duo/dailyWins"_json_pointer].dump();
		s_duo_tpp_weeklyWins = json["/data/attributes/gameModeStats/duo/weeklyWins"_json_pointer].dump();
		s_duo_tpp_damageDealt = json["/data/attributes/gameModeStats/duo/damageDealt"_json_pointer].dump();
		s_duo_tpp_headshotKills = json["/data/attributes/gameModeStats/duo/headshotKills"_json_pointer].dump();
		////duo_FPP
		s_duo_fpp_kills = json["/data/attributes/gameModeStats/duo-fpp/kills"_json_pointer].dump();
		s_duo_fpp_dBNOs = json["/data/attributes/gameModeStats/duo-fpp/dBNOs"_json_pointer].dump();
		s_duo_fpp_assists = json["/data/attributes/gameModeStats/duo-fpp/assists"_json_pointer].dump();
		s_duo_fpp_heals = json["/data/attributes/gameModeStats/duo-fpp/heals"_json_pointer].dump();
		s_duo_fpp_boosts = json["/data/attributes/gameModeStats/duo-fpp/boosts"_json_pointer].dump();
		s_duo_fpp_revives = json["/data/attributes/gameModeStats/duo-fpp/revives"_json_pointer].dump();
		s_duo_fpp_longestKill = json["/data/attributes/gameModeStats/duo-fpp/longestKill"_json_pointer].dump();
		s_duo_fpp_roundMostKills = json["/data/attributes/gameModeStats/duo-fpp/roundMostKills"_json_pointer].dump();
		s_duo_fpp_teamKills = json["/data/attributes/gameModeStats/duo-fpp/teamKills"_json_pointer].dump();
		s_duo_fpp_maxKillStreaks = json["/data/attributes/gameModeStats/duo-fpp/maxKillStreaks"_json_pointer].dump();
		s_duo_fpp_weaponsAcquired = json["/data/attributes/gameModeStats/duo-fpp/weaponsAcquired"_json_pointer].dump();
		s_duo_fpp_wins = json["/data/attributes/gameModeStats/duo-fpp/wins"_json_pointer].dump();
		s_duo_fpp_top10s = json["/data/attributes/gameModeStats/duo-fpp/top10s"_json_pointer].dump();
		s_duo_fpp_dailyKills = json["/data/attributes/gameModeStats/duo-fpp/dailyKills"_json_pointer].dump();
		s_duo_fpp_weeklyKills = json["/data/attributes/gameModeStats/duo-fpp/weeklyKills"_json_pointer].dump();
		s_duo_fpp_dailyWins = json["/data/attributes/gameModeStats/duo-fpp/dailyWins"_json_pointer].dump();
		s_duo_fpp_weeklyWins = json["/data/attributes/gameModeStats/duo-fpp/weeklyWins"_json_pointer].dump();
		s_duo_fpp_damageDealt = json["/data/attributes/gameModeStats/duo-fpp/damageDealt"_json_pointer].dump();
		s_duo_fpp_headshotKills = json["/data/attributes/gameModeStats/duo-fpp/headshotKills"_json_pointer].dump();
		////solo_TPP
		s_solo_tpp_kills = json["/data/attributes/gameModeStats/solo/kills"_json_pointer].dump();
		s_solo_tpp_dBNOs = json["/data/attributes/gameModeStats/solo/dBNOs"_json_pointer].dump();
		s_solo_tpp_assists = json["/data/attributes/gameModeStats/solo/assists"_json_pointer].dump();
		s_solo_tpp_heals = json["/data/attributes/gameModeStats/solo/heals"_json_pointer].dump();
		s_solo_tpp_boosts = json["/data/attributes/gameModeStats/solo/boosts"_json_pointer].dump();
		s_solo_tpp_revives = json["/data/attributes/gameModeStats/solo/revives"_json_pointer].dump();
		s_solo_tpp_longestKill = json["/data/attributes/gameModeStats/solo/longestKill"_json_pointer].dump();
		s_solo_tpp_roundMostKills = json["/data/attributes/gameModeStats/solo/roundMostKills"_json_pointer].dump();
		s_solo_tpp_teamKills = json["/data/attributes/gameModeStats/solo/teamKills"_json_pointer].dump();
		s_solo_tpp_maxKillStreaks = json["/data/attributes/gameModeStats/solo/maxKillStreaks"_json_pointer].dump();
		s_solo_tpp_weaponsAcquired = json["/data/attributes/gameModeStats/solo/weaponsAcquired"_json_pointer].dump();
		s_solo_tpp_wins = json["/data/attributes/gameModeStats/solo/wins"_json_pointer].dump();
		s_solo_tpp_top10s = json["/data/attributes/gameModeStats/solo/top10s"_json_pointer].dump();
		s_solo_tpp_dailyKills = json["/data/attributes/gameModeStats/solo/dailyKills"_json_pointer].dump();
		s_solo_tpp_weeklyKills = json["/data/attributes/gameModeStats/solo/weeklyKills"_json_pointer].dump();
		s_solo_tpp_dailyWins = json["/data/attributes/gameModeStats/solo/dailyWins"_json_pointer].dump();
		s_solo_tpp_weeklyWins = json["/data/attributes/gameModeStats/solo/weeklyWins"_json_pointer].dump();
		s_solo_tpp_damageDealt = json["/data/attributes/gameModeStats/solo/damageDealt"_json_pointer].dump();
		s_solo_tpp_headshotKills = json["/data/attributes/gameModeStats/solo/headshotKills"_json_pointer].dump();
		////solo_FPP
		s_solo_fpp_kills = json["/data/attributes/gameModeStats/solo-fpp/kills"_json_pointer].dump();
		s_solo_fpp_dBNOs = json["/data/attributes/gameModeStats/solo-fpp/dBNOs"_json_pointer].dump();
		s_solo_fpp_assists = json["/data/attributes/gameModeStats/solo-fpp/assists"_json_pointer].dump();
		s_solo_fpp_heals = json["/data/attributes/gameModeStats/solo-fpp/heals"_json_pointer].dump();
		s_solo_fpp_boosts = json["/data/attributes/gameModeStats/solo-fpp/boosts"_json_pointer].dump();
		s_solo_fpp_revives = json["/data/attributes/gameModeStats/solo-fpp/revives"_json_pointer].dump();
		s_solo_fpp_longestKill = json["/data/attributes/gameModeStats/solo-fpp/longestKill"_json_pointer].dump();
		s_solo_fpp_roundMostKills = json["/data/attributes/gameModeStats/solo-fpp/roundMostKills"_json_pointer].dump();
		s_solo_fpp_teamKills = json["/data/attributes/gameModeStats/solo-fpp/teamKills"_json_pointer].dump();
		s_solo_fpp_maxKillStreaks = json["/data/attributes/gameModeStats/solo-fpp/maxKillStreaks"_json_pointer].dump();
		s_solo_fpp_weaponsAcquired = json["/data/attributes/gameModeStats/solo-fpp/weaponsAcquired"_json_pointer].dump();
		s_solo_fpp_wins = json["/data/attributes/gameModeStats/solo-fpp/wins"_json_pointer].dump();
		s_solo_fpp_top10s = json["/data/attributes/gameModeStats/solo-fpp/top10s"_json_pointer].dump();
		s_solo_fpp_dailyKills = json["/data/attributes/gameModeStats/solo-fpp/dailyKills"_json_pointer].dump();
		s_solo_fpp_weeklyKills = json["/data/attributes/gameModeStats/solo-fpp/weeklyKills"_json_pointer].dump();
		s_solo_fpp_dailyWins = json["/data/attributes/gameModeStats/solo-fpp/dailyWins"_json_pointer].dump();
		s_solo_fpp_weeklyWins = json["/data/attributes/gameModeStats/solo-fpp/weeklyWins"_json_pointer].dump();
		s_solo_fpp_damageDealt = json["/data/attributes/gameModeStats/solo-fpp/damageDealt"_json_pointer].dump();
		s_solo_fpp_headshotKills = json["/data/attributes/gameModeStats/solo-fpp/headshotKills"_json_pointer].dump();
		////squad_TPP
		s_squad_tpp_kills = json["/data/attributes/gameModeStats/squad/kills"_json_pointer].dump();
		s_squad_tpp_dBNOs = json["/data/attributes/gameModeStats/squad/dBNOs"_json_pointer].dump();
		s_squad_tpp_assists = json["/data/attributes/gameModeStats/squad/assists"_json_pointer].dump();
		s_squad_tpp_heals = json["/data/attributes/gameModeStats/squad/heals"_json_pointer].dump();
		s_squad_tpp_boosts = json["/data/attributes/gameModeStats/squad/boosts"_json_pointer].dump();
		s_squad_tpp_revives = json["/data/attributes/gameModeStats/squad/revives"_json_pointer].dump();
		s_squad_tpp_longestKill = json["/data/attributes/gameModeStats/squad/longestKill"_json_pointer].dump();
		s_squad_tpp_roundMostKills = json["/data/attributes/gameModeStats/squad/roundMostKills"_json_pointer].dump();
		s_squad_tpp_teamKills = json["/data/attributes/gameModeStats/squad/teamKills"_json_pointer].dump();
		s_squad_tpp_maxKillStreaks = json["/data/attributes/gameModeStats/squad/maxKillStreaks"_json_pointer].dump();
		s_squad_tpp_weaponsAcquired = json["/data/attributes/gameModeStats/squad/weaponsAcquired"_json_pointer].dump();
		s_squad_tpp_wins = json["/data/attributes/gameModeStats/squad/wins"_json_pointer].dump();
		s_squad_tpp_top10s = json["/data/attributes/gameModeStats/squad/top10s"_json_pointer].dump();
		s_squad_tpp_dailyKills = json["/data/attributes/gameModeStats/squad/dailyKills"_json_pointer].dump();
		s_squad_tpp_weeklyKills = json["/data/attributes/gameModeStats/squad/weeklyKills"_json_pointer].dump();
		s_squad_tpp_dailyWins = json["/data/attributes/gameModeStats/squad/dailyWins"_json_pointer].dump();
		s_squad_tpp_weeklyWins = json["/data/attributes/gameModeStats/squad/weeklyWins"_json_pointer].dump();
		s_squad_tpp_damageDealt = json["/data/attributes/gameModeStats/squad/damageDealt"_json_pointer].dump();
		s_squad_tpp_headshotKills = json["/data/attributes/gameModeStats/squad/headshotKills"_json_pointer].dump();
		////squad_FPP
		s_squad_fpp_kills = json["/data/attributes/gameModeStats/squad-fpp/kills"_json_pointer].dump();
		s_squad_fpp_dBNOs = json["/data/attributes/gameModeStats/squad-fpp/dBNOs"_json_pointer].dump();
		s_squad_fpp_assists = json["/data/attributes/gameModeStats/squad-fpp/assists"_json_pointer].dump();
		s_squad_fpp_heals = json["/data/attributes/gameModeStats/squad-fpp/heals"_json_pointer].dump();
		s_squad_fpp_boosts = json["/data/attributes/gameModeStats/squad-fpp/boosts"_json_pointer].dump();
		s_squad_fpp_revives = json["/data/attributes/gameModeStats/squad-fpp/revives"_json_pointer].dump();
		s_squad_fpp_longestKill = json["/data/attributes/gameModeStats/squad-fpp/longestKill"_json_pointer].dump();
		s_squad_fpp_roundMostKills = json["/data/attributes/gameModeStats/squad-fpp/roundMostKills"_json_pointer].dump();
		s_squad_fpp_teamKills = json["/data/attributes/gameModeStats/squad-fpp/teamKills"_json_pointer].dump();
		s_squad_fpp_maxKillStreaks = json["/data/attributes/gameModeStats/squad-fpp/maxKillStreaks"_json_pointer].dump();
		s_squad_fpp_weaponsAcquired = json["/data/attributes/gameModeStats/squad-fpp/weaponsAcquired"_json_pointer].dump();
		s_squad_fpp_wins = json["/data/attributes/gameModeStats/squad-fpp/wins"_json_pointer].dump();
		s_squad_fpp_top10s = json["/data/attributes/gameModeStats/squad-fpp/top10s"_json_pointer].dump();
		s_squad_fpp_dailyKills = json["/data/attributes/gameModeStats/squad-fpp/dailyKills"_json_pointer].dump();
		s_squad_fpp_weeklyKills = json["/data/attributes/gameModeStats/squad-fpp/weeklyKills"_json_pointer].dump();
		s_squad_fpp_dailyWins = json["/data/attributes/gameModeStats/squad-fpp/dailyWins"_json_pointer].dump();
		s_squad_fpp_weeklyWins = json["/data/attributes/gameModeStats/squad-fpp/weeklyWins"_json_pointer].dump();
		s_squad_fpp_damageDealt = json["/data/attributes/gameModeStats/squad-fpp/damageDealt"_json_pointer].dump();
		s_squad_fpp_headshotKills = json["/data/attributes/gameModeStats/squad-fpp/headshotKills"_json_pointer].dump();
		//SEASON_STATS
	}

	void Season::displayAllStats()
	{
		//SEASON_STATS
		cout << "Kills : " << s_duo_tpp_kills << "\n";
		cout << "dBNOs : " << s_duo_tpp_dBNOs << "\n";
		cout << "Assists : " << s_duo_tpp_assists << "\n";
		cout << "Heals : " << s_duo_tpp_heals << "\n";
		cout << "Boosts : " << s_duo_tpp_boosts << "\n";
		cout << "Revives : " << s_duo_tpp_revives << "\n";
		cout << "Longest Kill : " << s_duo_tpp_longestKill << "\n";
		cout << "Round Most Kill : " << s_duo_tpp_roundMostKills << "\n";
		cout << "Team Kills : " << s_duo_tpp_teamKills << "\n";
		cout << "Max Kill Streaks : " << s_duo_tpp_maxKillStreaks << "\n";
		cout << "W. Acquired : " << s_duo_tpp_weaponsAcquired << "\n";
		cout << "Wins : " << s_duo_tpp_wins << "\n";
		cout << "Top10s : " << s_duo_tpp_top10s << "\n";
		cout << "DailyKills : " << s_duo_tpp_dailyKills << "\n";
		cout << "WeeklyKills : " << s_duo_tpp_weeklyKills << "\n";
		cout << "DailyWins : " << s_duo_tpp_dailyWins << "\n";
		cout << "WeeklyWins : " << s_duo_tpp_weeklyWins << "\n";
		cout << "Damage Dealt : " << s_duo_tpp_damageDealt << "\n";
		cout << "Headshot Kills: " << s_duo_tpp_headshotKills << "\n";
		cout << "\n\n";
		cout << "Kills : " << s_duo_fpp_kills << "\n";
		cout << "DBNOs : " << s_duo_fpp_dBNOs << "\n";
		cout << "Assists : " << s_duo_fpp_assists << "\n";
		cout << "Heals : " << s_duo_fpp_heals << "\n";
		cout << "Boosts : " << s_duo_fpp_boosts << "\n";
		cout << "Rezs : " << s_duo_fpp_revives << "\n";
		cout << "Longest Kill : " << s_duo_fpp_longestKill << "\n";
		cout << "Roudn Most Kills : " << s_duo_fpp_roundMostKills << "\n";
		cout << "Team Kills : " << s_duo_fpp_teamKills << "\n";
		cout << "Max Kill Streak : " << s_duo_fpp_maxKillStreaks << "\n";
		cout << "W. acquired : " << s_duo_fpp_weaponsAcquired << "\n";
		cout << "Wins : " << s_duo_fpp_wins << "\n";
		cout << "Top10s : " << s_duo_fpp_top10s << "\n";
		cout << "DailyKills : " << s_duo_fpp_dailyKills << "\n";
		cout << "WeeklyKills : " << s_duo_fpp_weeklyKills << "\n";
		cout << "DailyWins : " << s_duo_fpp_dailyWins << "\n";
		cout << "WeeklyKills : " << s_duo_fpp_weeklyWins << "\n";
		cout << "Damage Dealt : " << s_duo_fpp_damageDealt << "\n";
		cout << "Headshot Kills : " << s_duo_fpp_headshotKills << "\n";
		cout << "\n\n";
		cout << "Kills : " << s_solo_tpp_kills << "\n";
		cout << "dBNOs : " << s_solo_tpp_dBNOs << "\n";
		cout << "Assists : " << s_solo_tpp_assists << "\n";
		cout << "Heals : " << s_solo_tpp_heals << "\n";
		cout << "Boosts : " << s_solo_tpp_boosts << "\n";
		cout << "Revives : " << s_solo_tpp_revives << "\n";
		cout << "Longest Kill : " << s_solo_tpp_longestKill << "\n";
		cout << "Round Most Kill : " << s_solo_tpp_roundMostKills << "\n";
		cout << "Team Kills : " << s_solo_tpp_teamKills << "\n";
		cout << "Max Kill Streaks : " << s_solo_tpp_maxKillStreaks << "\n";
		cout << "W. Acquired : " << s_solo_tpp_weaponsAcquired << "\n";
		cout << "Wins : " << s_solo_tpp_wins << "\n";
		cout << "Top10s : " << s_solo_tpp_top10s << "\n";
		cout << "DailyKills : " << s_solo_tpp_dailyKills << "\n";
		cout << "WeeklyKills : " << s_solo_tpp_weeklyKills << "\n";
		cout << "DailyWins : " << s_solo_tpp_dailyWins << "\n";
		cout << "WeeklyWins : " << s_solo_tpp_weeklyWins << "\n";
		cout << "Damage Dealt : " << s_solo_tpp_damageDealt << "\n";
		cout << "Headshot Kills: " << s_solo_tpp_headshotKills << "\n";
		cout << "\n\n";
		cout << "Kills : " << s_solo_fpp_kills << "\n";
		cout << "DBNOs : " << s_solo_fpp_dBNOs << "\n";
		cout << "Assists : " << s_solo_fpp_assists << "\n";
		cout << "Heals : " << s_solo_fpp_heals << "\n";
		cout << "Boosts : " << s_solo_fpp_boosts << "\n";
		cout << "Rezs : " << s_solo_fpp_revives << "\n";
		cout << "Longest Kill : " << s_solo_fpp_longestKill << "\n";
		cout << "Roudn Most Kills : " << s_solo_fpp_roundMostKills << "\n";
		cout << "Team Kills : " << s_solo_fpp_teamKills << "\n";
		cout << "Max Kill Streak : " << s_solo_fpp_maxKillStreaks << "\n";
		cout << "W. acquired : " << s_solo_fpp_weaponsAcquired << "\n";
		cout << "Wins : " << s_solo_fpp_wins << "\n";
		cout << "Top10s : " << s_solo_fpp_top10s << "\n";
		cout << "DailyKills : " << s_solo_fpp_dailyKills << "\n";
		cout << "WeeklyKills : " << s_solo_fpp_weeklyKills << "\n";
		cout << "DailyWins : " << s_solo_fpp_dailyWins << "\n";
		cout << "WeeklyKills : " << s_solo_fpp_weeklyWins << "\n";
		cout << "Damage Dealt : " << s_solo_fpp_damageDealt << "\n";
		cout << "Headshot Kills : " << s_solo_fpp_headshotKills << "\n";
		cout << "\n\n";
		cout << "Kills : " << s_squad_tpp_kills << "\n";
		cout << "dBNOs : " << s_squad_tpp_dBNOs << "\n";
		cout << "Assists : " << s_squad_tpp_assists << "\n";
		cout << "Heals : " << s_squad_tpp_heals << "\n";
		cout << "Boosts : " << s_squad_tpp_boosts << "\n";
		cout << "Revives : " << s_squad_tpp_revives << "\n";
		cout << "Longest Kill : " << s_squad_tpp_longestKill << "\n";
		cout << "Round Most Kill : " << s_squad_tpp_roundMostKills << "\n";
		cout << "Team Kills : " << s_squad_tpp_teamKills << "\n";
		cout << "Max Kill Streaks : " << s_squad_tpp_maxKillStreaks << "\n";
		cout << "W. Acquired : " << s_squad_tpp_weaponsAcquired << "\n";
		cout << "Wins : " << s_squad_tpp_wins << "\n";
		cout << "Top10s : " << s_squad_tpp_top10s << "\n";
		cout << "DailyKills : " << s_squad_tpp_dailyKills << "\n";
		cout << "WeeklyKills : " << s_squad_tpp_weeklyKills << "\n";
		cout << "DailyWins : " << s_squad_tpp_dailyWins << "\n";
		cout << "WeeklyWins : " << s_squad_tpp_weeklyWins << "\n";
		cout << "Damage Dealt : " << s_squad_tpp_damageDealt << "\n";
		cout << "Headshot Kills: " << s_squad_tpp_headshotKills << "\n";
		cout << "\n\n";
		cout << "Kills : " << s_squad_fpp_kills << "\n";
		cout << "DBNOs : " << s_squad_fpp_dBNOs << "\n";
		cout << "Assists : " << s_squad_fpp_assists << "\n";
		cout << "Heals : " << s_squad_fpp_heals << "\n";
		cout << "Boosts : " << s_squad_fpp_boosts << "\n";
		cout << "Rezs : " << s_squad_fpp_revives << "\n";
		cout << "Longest Kill : " << s_squad_fpp_longestKill << "\n";
		cout << "Roudn Most Kills : " << s_squad_fpp_roundMostKills << "\n";
		cout << "Team Kills : " << s_squad_fpp_teamKills << "\n";
		cout << "Max Kill Streak : " << s_squad_fpp_maxKillStreaks << "\n";
		cout << "W. acquired : " << s_squad_fpp_weaponsAcquired << "\n";
		cout << "Wins : " << s_squad_fpp_wins << "\n";
		cout << "Top10s : " << s_squad_fpp_top10s << "\n";
		cout << "DailyKills : " << s_squad_fpp_dailyKills << "\n";
		cout << "WeeklyKills : " << s_squad_fpp_weeklyKills << "\n";
		cout << "DailyWins : " << s_squad_fpp_dailyWins << "\n";
		cout << "WeeklyKills : " << s_squad_fpp_weeklyWins << "\n";
		cout << "Damage Dealt : " << s_squad_fpp_damageDealt << "\n";
		cout << "Headshot Kills : " << s_squad_fpp_headshotKills << "\n";
		//SEASON_STATS
	}
}