#pragma once

#include <string>

#include <nlohmann/json.hpp>

#include "cURL_functions.h"

#define SMOL_BUFFER 256
#define BUFFER_SIZE 4096

#define CURRENT_SEASON_ID "division.bro.official.pc-2018-02"

namespace DogtagCore {
	class User
	{
	public:
		User();
		User(std::string name);
		~User();

		std::string		getName() const;
		std::string		getId() const;
		void			parseUserInfo();
		void			writeUserInfo();
		void			setUserData(int verbose);
		CURL_data		getUserData();
		nlohmann::json	getJson();
		std::string		getLastMatchId();
		std::string		getCurrentSeasonId();
		bool			getHasJson();

	private:
		std::string		u_name;
		std::string		u_id;
		std::string		u_filename;
		CURL_data		u_curl_data;
		nlohmann::json	u_json;
		std::string		u_last_match_id;
		std::string		u_current_season_id;
		bool			u_has_json;
	};
}