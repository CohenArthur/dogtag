#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>

#include "curl/curl.h"

#include "cURL_functions.h"

#define API_KEY "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJkMzliYjQwMC0xYzQwLTAxMzctYmJlYi0wN2Q3ZGU1ZTNiZDEiLCJpc3MiOiJnYW1lbG9ja2VyIiwiaWF0IjoxNTUxMjE4ODQ3LCJwdWIiOiJibHVlaG9sZSIsInRpdGxlIjoicHViZyIsImFwcCI6ImppbmtvYXBwIn0.Sdg9NAis_g8dkLyqgNwDiksMvgYQT9tynrMEXPFKEw4"

//Callback function necessary to libcURL so we can print the data received through cout or inside a file.
size_t writeMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;
	CURL_data *mem = (CURL_data *)userp;

	char *ptr = (char *)realloc(mem->data, mem->size + realsize + 1);

	if (ptr == NULL) //Check for a realloc fail.
	{
		std::cout << "not enough memory (realloc returned NULL)\n";
		return 0;
	}

	mem->data = ptr;
	memcpy(&(mem->data[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->data[mem->size] = 0;

	return realsize;
}

//Simply inits the data structure. Called by performRequest().
CURL_data initC_d()
{
	CURL_data new_data;

	char *string = (char *)malloc(1);
	if (!string)
		std::cerr << "Allocating (new_data.data) failed." << std::endl;

	new_data.data		= string;
	new_data.size		= 1;
	
	return new_data;
}

//Removes the first bit of the received string, as it is a garbage char, and outputs the contents of the string to a .json file.
void cleanDataC_d(CURL_data data)
{
	memmove(data.data, data.data + 1, strlen(data.data + 1) + 1);

	std::string data_string = std::string(data.data);
	std::ofstream req_file(data.filename); //../user_filename.json

	req_file << data_string;
}

//The main function of this file : Sets all options before performing the curl_easy_perform() call and storing it in a CURL_data structure.
CURL_data performRequest(int verbose, int type, CURL_request request)
{
	CURL *handle = curl_easy_init();

	CURLcode curl_error;

	CURL_data new_data = initC_d();

	if (verbose)
		curl_easy_setopt(handle, CURLOPT_VERBOSE, 1L);

	struct curl_slist *slist = NULL;

	char api_key[BUFFER_SIZE] = API_KEY;
	char api_header[BUFFER_SIZE] = "\0";
	char api_header_head[SMOL_BUFFER] = "Authorization: Bearer ";

	strcat_s(api_header, sizeof(api_header), api_header_head);
	strcat_s(api_header, sizeof(api_header), api_key);

	slist = curl_slist_append(slist, api_header);
	slist = curl_slist_append(slist, "Accept: application/vnd.api+json");

	curl_easy_setopt(handle, CURLOPT_HTTPHEADER, slist);

	curl_easy_setopt(handle, CURLOPT_SSL_VERIFYHOST, 0L);
	curl_easy_setopt(handle, CURLOPT_SSL_VERIFYPEER, 0L);

	curl_easy_setopt(handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

	switch (type)
	{
		case USER:
		{
			new_data.filename = "player_request_result.json";

			char url_str[BUFFER_SIZE] = "https://api.pubg.com/shards/steam/players?filter[playerNames]=";

			strcat_s(url_str, sizeof(url_str), request.name.c_str());
			
			curl_easy_setopt(handle, CURLOPT_URL, url_str);

			break;
		}

		case MATCH:
		{	
			new_data.filename = "match_request_result.json";

			char url_str[BUFFER_SIZE] = "https://api.pubg.com/shards/steam/matches/";

			strcat_s(url_str, sizeof(url_str), request.last_match_id.c_str());

			curl_easy_setopt(handle, CURLOPT_URL, url_str);

			break;
		}

		case SEASONS:
		{
			new_data.filename = "seasons_request_result.json";

			char url_str[BUFFER_SIZE] = "https://api.pubg.com/shards/steam/seasons";

			curl_easy_setopt(handle, CURLOPT_URL, url_str);

			break;
		}

		case USER_SEASON:
		{
			new_data.filename = "user_season_request_result.json";

			char url_str[BUFFER_SIZE] = "https://api.pubg.com/shards/steam/players/";

			strcat_s(url_str, sizeof(url_str), request.id.c_str());
			strcat_s(url_str, sizeof(url_str), "/seasons/");
			strcat_s(url_str, sizeof(url_str), request.current_season_id.c_str());

			curl_easy_setopt(handle, CURLOPT_URL, url_str);

			break;
		}

		case LIFETIME:
		{
			new_data.filename = "lifetime_request_result.json";

			char url_str[BUFFER_SIZE] = "https://api.pubg.com/shards/steam/players/";

			strcat_s(url_str, sizeof(url_str), request.id.c_str());
			strcat_s(url_str, sizeof(url_str), "/seasons/lifetime");

			curl_easy_setopt(handle, CURLOPT_URL, url_str);

			break;
		}

		default:
		{
			std::cerr << "Type not specified when calling performRequest()" << std::endl;
		}
	}

	//END SWITCH_CASE

	curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writeMemoryCallback);
	curl_easy_setopt(handle, CURLOPT_WRITEDATA, (void *)&new_data);

	curl_error = curl_easy_perform(handle);
	if (curl_error != CURLE_OK) //CURLE_OK = 0
		fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(curl_error));
	else {
		if (verbose)
		{
			std::cout << new_data.size << " bytes retrieved\n";
			std::cout << "Raw Data :\n" << new_data.data << "\n";
		}
	}

	curl_slist_free_all(slist);
	curl_easy_cleanup(handle);

	cleanDataC_d(new_data);

	return new_data;
}