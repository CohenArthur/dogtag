#pragma once

#include <string>

namespace DogtagCore {
	class Grip
	{
	public:
		Grip(std::string grip_name);
		~Grip();

		std::string getName();
		std::string getRecoilPattern();
		std::string getHorizontalRecoil();
		std::string getVerticalRecoil();
		std::string getRecovery();
		std::string getPelletSpread();
		std::string getDeviation();

	private:
		std::string g_name;
		std::string g_recoil_pattern;
		std::string g_hori_recoil;
		std::string g_vert_recoil;
		std::string g_recovery;
		std::string g_pellet_spread;
		std::string g_deviation;
	};
}
