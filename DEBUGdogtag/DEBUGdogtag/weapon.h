#pragma once

#include <string>

namespace DogtagCore {
	class Weapon
	{
	public:
		Weapon(std::string weapon_name);

		//void setAllData();

		std::string getName();
		std::string getHitDamage();
		std::string getBulletSpeed();
		std::string getDPS();
		std::string getMagSize();
		std::string getTimeBetweenShots();
		std::string getAmmoType();

		~Weapon();

	private:
		std::string w_name;
		std::string w_hit_damage;
		std::string w_bullet_speed;
		std::string w_dps;
		std::string w_mag_size;
		std::string w_time_per_shot;
		std::string w_ammo;
	};
}