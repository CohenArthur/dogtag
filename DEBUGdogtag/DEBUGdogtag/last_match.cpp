#include "last_match.h"

#include "user.h"

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <nlohmann/json.hpp>

using namespace std;
using json = nlohmann::json;

namespace DogtagCore {
	LastMatch::LastMatch(string last_match_id, string user_name)
	{
		//string id = user.getLastMatchId();
		if (last_match_id == "\0")
			cerr << "User has no u_last_match_id member set. Perform user.setUserData() and try again.";
		lm_id = last_match_id;
		string name = user_name;

		lm_name.append("\"");
		lm_name.append(name);
		lm_name.append("\"");
	}

	LastMatch::~LastMatch()
	{
	}

	void LastMatch::setLastMatchInfo(int verbose)
	{
		CURL_request request;
		request.last_match_id = lm_id;
		CURL_data data = performRequest(verbose, MATCH, request);

		std::ifstream json_req(data.filename);
		json new_json = json::parse(json_req);

		lm_curl_data = data;
		lm_json = new_json;

		setAllData();
	}

	json LastMatch::getJson()
	{
		return lm_json;
	}

	CURL_data LastMatch::getLastMatchData()
	{
		return lm_curl_data;
	}

	void LastMatch::setAllData()
	{
		size_t index = 0;

		while (true)
		{
			if (lm_json["/included"_json_pointer][index]["/attributes/stats/name"_json_pointer].dump().compare(lm_name) == 0)
				break;
			index++;
		}

		lm_rank = lm_json["/included"_json_pointer][index]["/attributes/stats/winPlace"_json_pointer].dump();
		lm_kills = lm_json["/included"_json_pointer][index]["/attributes/stats/kills"_json_pointer].dump();
		lm_dbnos = lm_json["/included"_json_pointer][index]["/attributes/stats/DBNOs"_json_pointer].dump();
		lm_assists = lm_json["/included"_json_pointer][index]["/attributes/stats/assists"_json_pointer].dump();
		lm_killRank = lm_json["/included"_json_pointer][index]["/attributes/stats/killPlace"_json_pointer].dump();
		lm_damageDealt = lm_json["/included"_json_pointer][index]["/attributes/stats/damageDealt"_json_pointer].dump();
		lm_headshotKills = lm_json["/included"_json_pointer][index]["/attributes/stats/headshotKills"_json_pointer].dump();
		lm_heals = lm_json["/included"_json_pointer][index]["/attributes/stats/heals"_json_pointer].dump();
		lm_boosts = lm_json["/included"_json_pointer][index]["/attributes/stats/boosts"_json_pointer].dump();
		lm_longestKill = lm_json["/included"_json_pointer][index]["/attributes/stats/longestKill"_json_pointer].dump();
		lm_timeSurvived = lm_json["/included"_json_pointer][index]["/attributes/stats/timeSurvived"_json_pointer].dump();
		lm_revives = lm_json["/included"_json_pointer][index]["/attributes/stats/revives"_json_pointer].dump();
		lm_rideDistance = lm_json["/included"_json_pointer][index]["/attributes/stats/rideDistance"_json_pointer].dump();
		lm_roadKills = lm_json["/included"_json_pointer][index]["/attributes/stats/roadKills"_json_pointer].dump();
		lm_teamKills = lm_json["/included"_json_pointer][index]["/attributes/stats/teamKills"_json_pointer].dump();
		lm_vehicleDestroys = lm_json["/included"_json_pointer][index]["/attributes/stats/vehicleDestroys"_json_pointer].dump();
		lm_walkDistance = lm_json["/included"_json_pointer][index]["/attributes/stats/walkDistance"_json_pointer].dump();
		lm_weaponsAcquired = lm_json["/included"_json_pointer][index]["/attributes/stats/weaponsAcquired"_json_pointer].dump();
	}

	void LastMatch::displayAllData()
	{
		cout << "Rank : " << lm_rank << "\n";
		cout << "Kills : " << lm_kills << "\n";
		cout << "DBNOs : " << lm_dbnos << "\n";
		cout << "Assists : " << lm_assists << "\n";
		cout << "KillRank : " << lm_killRank << "\n";
		cout << "DamageDealt : " << lm_damageDealt << "\n";
		cout << "HSKills : " << lm_headshotKills << "\n";
		cout << "Heals : " << lm_heals << "\n";
		cout << "Boosts : " << lm_boosts << "\n";
		cout << "LongestKill : " << lm_longestKill << "\n";
		cout << "TimeSurvived : " << lm_timeSurvived << "\n";
		cout << "Revives : " << lm_revives << "\n";
		cout << "RideDistance : " << lm_rideDistance << "\n";
		cout << "RoadKills : " << lm_roadKills << "\n";
		cout << "TeamKills : " << lm_teamKills << "\n";
		cout << "VehicleDestroys : " << lm_vehicleDestroys << "\n";
		cout << "WalkDistance : " << lm_walkDistance << "\n";
		cout << "WeaponsAcquired : " << lm_weaponsAcquired << "\n";
	}
}