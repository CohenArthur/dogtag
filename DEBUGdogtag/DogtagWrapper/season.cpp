#include "season.h"

//WRAPPER CLASS

namespace DogtagCLI {
	Season::Season()
		: ManagedObject(new DogtagCore::Season()) {

	}

	Season::Season(String^ id, int timeline)
		: ManagedObject(new DogtagCore::Season(stringToCharArray(id), timeline)) {

	}

	void Season::setAllSeasonsData(int verbose) {
		try {
			m_instance->setAllSeasonsData(verbose);
		}
		catch (Exception^ ex) {
			throw gcnew System::Exception(gcnew String("Fetchin AllDataSeasons failed, probably due to too many API Calls. Please try again in a bit !"));
		}
	}

	void Season::setUserSeasonData(int verbose, int timeline) {
		try {
			m_instance->setUserSeasonData(verbose, timeline);
		}
		catch (Exception^ ex) {
			throw gcnew System::Exception(gcnew String("Setting Data for User Season failed, probably due to too many API Calls. Please try again in a bit !"));
		}
	}

	void Season::displayAllStats() {
		m_instance->displayAllStats();
	}

	String^ Season::getduo_tpp_kills() { return gcnew String(m_instance->s_duo_tpp_kills.c_str()); }
	String^ Season::getduo_tpp_dBNOs() { return gcnew String(m_instance->s_duo_tpp_dBNOs.c_str()); }
	String^ Season::getduo_tpp_assists() { return gcnew String(m_instance->s_duo_tpp_assists.c_str()); }
	String^ Season::getduo_tpp_heals() { return gcnew String(m_instance->s_duo_tpp_heals.c_str()); }
	String^ Season::getduo_tpp_boosts() { return gcnew String(m_instance->s_duo_tpp_boosts.c_str()); }
	String^ Season::getduo_tpp_revives() { return gcnew String(m_instance->s_duo_tpp_revives.c_str()); }
	String^ Season::getduo_tpp_longestKill() { return gcnew String(m_instance->s_duo_tpp_longestKill.c_str()); }
	String^ Season::getduo_tpp_roundMostKills() { return gcnew String(m_instance->s_duo_tpp_roundMostKills.c_str()); }
	String^ Season::getduo_tpp_teamKills() { return gcnew String(m_instance->s_duo_tpp_teamKills.c_str()); }
	String^ Season::getduo_tpp_maxKillStreaks() { return gcnew String(m_instance->s_duo_tpp_maxKillStreaks.c_str()); }
	String^ Season::getduo_tpp_weaponsAcquired() { return gcnew String(m_instance->s_duo_tpp_weaponsAcquired.c_str()); }
	String^ Season::getduo_tpp_wins() { return gcnew String(m_instance->s_duo_tpp_wins.c_str()); }
	String^ Season::getduo_tpp_top10s() { return gcnew String(m_instance->s_duo_tpp_top10s.c_str()); }
	String^ Season::getduo_tpp_dailyKills() { return gcnew String(m_instance->s_duo_tpp_dailyKills.c_str()); }
	String^ Season::getduo_tpp_weeklyKills() { return gcnew String(m_instance->s_duo_tpp_weeklyKills.c_str()); }
	String^ Season::getduo_tpp_dailyWins() { return gcnew String(m_instance->s_duo_tpp_dailyWins.c_str()); }
	String^ Season::getduo_tpp_weeklyWins() { return gcnew String(m_instance->s_duo_tpp_weeklyWins.c_str()); }
	String^ Season::getduo_tpp_damageDealt() { return gcnew String(m_instance->s_duo_tpp_damageDealt.c_str()); }
	String^ Season::getduo_tpp_headshotKills() { return gcnew String(m_instance->s_duo_tpp_headshotKills.c_str()); }

	String^ Season::getduo_fpp_kills() { return gcnew String(m_instance->s_duo_fpp_kills.c_str()); }
	String^ Season::getduo_fpp_dBNOs() { return gcnew String(m_instance->s_duo_fpp_dBNOs.c_str()); }
	String^ Season::getduo_fpp_assists() { return gcnew String(m_instance->s_duo_fpp_assists.c_str()); }
	String^ Season::getduo_fpp_heals() { return gcnew String(m_instance->s_duo_fpp_heals.c_str()); }
	String^ Season::getduo_fpp_boosts() { return gcnew String(m_instance->s_duo_fpp_boosts.c_str()); }
	String^ Season::getduo_fpp_revives() { return gcnew String(m_instance->s_duo_fpp_revives.c_str()); }
	String^ Season::getduo_fpp_longestKill() { return gcnew String(m_instance->s_duo_fpp_longestKill.c_str()); }
	String^ Season::getduo_fpp_roundMostKills() { return gcnew String(m_instance->s_duo_fpp_roundMostKills.c_str()); }
	String^ Season::getduo_fpp_teamKills() { return gcnew String(m_instance->s_duo_fpp_teamKills.c_str()); }
	String^ Season::getduo_fpp_maxKillStreaks() { return gcnew String(m_instance->s_duo_fpp_maxKillStreaks.c_str()); }
	String^ Season::getduo_fpp_weaponsAcquired() { return gcnew String(m_instance->s_duo_fpp_weaponsAcquired.c_str()); }
	String^ Season::getduo_fpp_wins() { return gcnew String(m_instance->s_duo_fpp_wins.c_str()); }
	String^ Season::getduo_fpp_top10s() { return gcnew String(m_instance->s_duo_fpp_top10s.c_str()); }
	String^ Season::getduo_fpp_dailyKills() { return gcnew String(m_instance->s_duo_fpp_dailyKills.c_str()); }
	String^ Season::getduo_fpp_weeklyKills() { return gcnew String(m_instance->s_duo_fpp_weeklyKills.c_str()); }
	String^ Season::getduo_fpp_dailyWins() { return gcnew String(m_instance->s_duo_fpp_dailyWins.c_str()); }
	String^ Season::getduo_fpp_weeklyWins() { return gcnew String(m_instance->s_duo_fpp_weeklyWins.c_str()); }
	String^ Season::getduo_fpp_damageDealt() { return gcnew String(m_instance->s_duo_fpp_damageDealt.c_str()); }
	String^ Season::getduo_fpp_headshotKills() { return gcnew String(m_instance->s_duo_fpp_headshotKills.c_str()); }

	String^ Season::getsolo_tpp_kills() { return gcnew String(m_instance->s_solo_tpp_kills.c_str()); }
	String^ Season::getsolo_tpp_dBNOs() { return gcnew String(m_instance->s_solo_tpp_dBNOs.c_str()); }
	String^ Season::getsolo_tpp_assists() { return gcnew String(m_instance->s_solo_tpp_assists.c_str()); }
	String^ Season::getsolo_tpp_heals() { return gcnew String(m_instance->s_solo_tpp_heals.c_str()); }
	String^ Season::getsolo_tpp_boosts() { return gcnew String(m_instance->s_solo_tpp_boosts.c_str()); }
	String^ Season::getsolo_tpp_revives() { return gcnew String(m_instance->s_solo_tpp_revives.c_str()); }
	String^ Season::getsolo_tpp_longestKill() { return gcnew String(m_instance->s_solo_tpp_longestKill.c_str()); }
	String^ Season::getsolo_tpp_roundMostKills() { return gcnew String(m_instance->s_solo_tpp_roundMostKills.c_str()); }
	String^ Season::getsolo_tpp_teamKills() { return gcnew String(m_instance->s_solo_tpp_teamKills.c_str()); }
	String^ Season::getsolo_tpp_maxKillStreaks() { return gcnew String(m_instance->s_solo_tpp_maxKillStreaks.c_str()); }
	String^ Season::getsolo_tpp_weaponsAcquired() { return gcnew String(m_instance->s_solo_tpp_weaponsAcquired.c_str()); }
	String^ Season::getsolo_tpp_wins() { return gcnew String(m_instance->s_solo_tpp_wins.c_str()); }
	String^ Season::getsolo_tpp_top10s() { return gcnew String(m_instance->s_solo_tpp_top10s.c_str()); }
	String^ Season::getsolo_tpp_dailyKills() { return gcnew String(m_instance->s_solo_tpp_dailyKills.c_str()); }
	String^ Season::getsolo_tpp_weeklyKills() { return gcnew String(m_instance->s_solo_tpp_weeklyKills.c_str()); }
	String^ Season::getsolo_tpp_dailyWins() { return gcnew String(m_instance->s_solo_tpp_dailyWins.c_str()); }
	String^ Season::getsolo_tpp_weeklyWins() { return gcnew String(m_instance->s_solo_tpp_weeklyWins.c_str()); }
	String^ Season::getsolo_tpp_damageDealt() { return gcnew String(m_instance->s_solo_tpp_damageDealt.c_str()); }
	String^ Season::getsolo_tpp_headshotKills() { return gcnew String(m_instance->s_solo_tpp_headshotKills.c_str()); }

	String^ Season::getsolo_fpp_kills() { return gcnew String(m_instance->s_solo_fpp_kills.c_str()); }
	String^ Season::getsolo_fpp_dBNOs() { return gcnew String(m_instance->s_solo_fpp_dBNOs.c_str()); }
	String^ Season::getsolo_fpp_assists() { return gcnew String(m_instance->s_solo_fpp_assists.c_str()); }
	String^ Season::getsolo_fpp_heals() { return gcnew String(m_instance->s_solo_fpp_heals.c_str()); }
	String^ Season::getsolo_fpp_boosts() { return gcnew String(m_instance->s_solo_fpp_boosts.c_str()); }
	String^ Season::getsolo_fpp_revives() { return gcnew String(m_instance->s_solo_fpp_revives.c_str()); }
	String^ Season::getsolo_fpp_longestKill() { return gcnew String(m_instance->s_solo_fpp_longestKill.c_str()); }
	String^ Season::getsolo_fpp_roundMostKills() { return gcnew String(m_instance->s_solo_fpp_roundMostKills.c_str()); }
	String^ Season::getsolo_fpp_teamKills() { return gcnew String(m_instance->s_solo_fpp_teamKills.c_str()); }
	String^ Season::getsolo_fpp_maxKillStreaks() { return gcnew String(m_instance->s_solo_fpp_maxKillStreaks.c_str()); }
	String^ Season::getsolo_fpp_weaponsAcquired() { return gcnew String(m_instance->s_solo_fpp_weaponsAcquired.c_str()); }
	String^ Season::getsolo_fpp_wins() { return gcnew String(m_instance->s_solo_fpp_wins.c_str()); }
	String^ Season::getsolo_fpp_top10s() { return gcnew String(m_instance->s_solo_fpp_top10s.c_str()); }
	String^ Season::getsolo_fpp_dailyKills() { return gcnew String(m_instance->s_solo_fpp_dailyKills.c_str()); }
	String^ Season::getsolo_fpp_weeklyKills() { return gcnew String(m_instance->s_solo_fpp_weeklyKills.c_str()); }
	String^ Season::getsolo_fpp_dailyWins() { return gcnew String(m_instance->s_solo_fpp_dailyWins.c_str()); }
	String^ Season::getsolo_fpp_weeklyWins() { return gcnew String(m_instance->s_solo_fpp_weeklyWins.c_str()); }
	String^ Season::getsolo_fpp_damageDealt() { return gcnew String(m_instance->s_solo_fpp_damageDealt.c_str()); }
	String^ Season::getsolo_fpp_headshotKills() { return gcnew String(m_instance->s_solo_fpp_headshotKills.c_str()); }

	String^ Season::getsquad_tpp_kills() { return gcnew String(m_instance->s_squad_tpp_kills.c_str()); }
	String^ Season::getsquad_tpp_dBNOs() { return gcnew String(m_instance->s_squad_tpp_dBNOs.c_str()); }
	String^ Season::getsquad_tpp_assists() { return gcnew String(m_instance->s_squad_tpp_assists.c_str()); }
	String^ Season::getsquad_tpp_heals() { return gcnew String(m_instance->s_squad_tpp_heals.c_str()); }
	String^ Season::getsquad_tpp_boosts() { return gcnew String(m_instance->s_squad_tpp_boosts.c_str()); }
	String^ Season::getsquad_tpp_revives() { return gcnew String(m_instance->s_squad_tpp_revives.c_str()); }
	String^ Season::getsquad_tpp_longestKill() { return gcnew String(m_instance->s_squad_tpp_longestKill.c_str()); }
	String^ Season::getsquad_tpp_roundMostKills() { return gcnew String(m_instance->s_squad_tpp_roundMostKills.c_str()); }
	String^ Season::getsquad_tpp_teamKills() { return gcnew String(m_instance->s_squad_tpp_teamKills.c_str()); }
	String^ Season::getsquad_tpp_maxKillStreaks() { return gcnew String(m_instance->s_squad_tpp_maxKillStreaks.c_str()); }
	String^ Season::getsquad_tpp_weaponsAcquired() { return gcnew String(m_instance->s_squad_tpp_weaponsAcquired.c_str()); }
	String^ Season::getsquad_tpp_wins() { return gcnew String(m_instance->s_squad_tpp_wins.c_str()); }
	String^ Season::getsquad_tpp_top10s() { return gcnew String(m_instance->s_squad_tpp_top10s.c_str()); }
	String^ Season::getsquad_tpp_dailyKills() { return gcnew String(m_instance->s_squad_tpp_dailyKills.c_str()); }
	String^ Season::getsquad_tpp_weeklyKills() { return gcnew String(m_instance->s_squad_tpp_weeklyKills.c_str()); }
	String^ Season::getsquad_tpp_dailyWins() { return gcnew String(m_instance->s_squad_tpp_dailyWins.c_str()); }
	String^ Season::getsquad_tpp_weeklyWins() { return gcnew String(m_instance->s_squad_tpp_weeklyWins.c_str()); }
	String^ Season::getsquad_tpp_damageDealt() { return gcnew String(m_instance->s_squad_tpp_damageDealt.c_str()); }
	String^ Season::getsquad_tpp_headshotKills() { return gcnew String(m_instance->s_squad_tpp_headshotKills.c_str()); }

	String^ Season::getsquad_fpp_kills() { return gcnew String(m_instance->s_squad_fpp_kills.c_str()); }
	String^ Season::getsquad_fpp_dBNOs() { return gcnew String(m_instance->s_squad_fpp_dBNOs.c_str()); }
	String^ Season::getsquad_fpp_assists() { return gcnew String(m_instance->s_squad_fpp_assists.c_str()); }
	String^ Season::getsquad_fpp_heals() { return gcnew String(m_instance->s_squad_fpp_heals.c_str()); }
	String^ Season::getsquad_fpp_boosts() { return gcnew String(m_instance->s_squad_fpp_boosts.c_str()); }
	String^ Season::getsquad_fpp_revives() { return gcnew String(m_instance->s_squad_fpp_revives.c_str()); }
	String^ Season::getsquad_fpp_longestKill() { return gcnew String(m_instance->s_squad_fpp_longestKill.c_str()); }
	String^ Season::getsquad_fpp_roundMostKills() { return gcnew String(m_instance->s_squad_fpp_roundMostKills.c_str()); }
	String^ Season::getsquad_fpp_teamKills() { return gcnew String(m_instance->s_squad_fpp_teamKills.c_str()); }
	String^ Season::getsquad_fpp_maxKillStreaks() { return gcnew String(m_instance->s_squad_fpp_maxKillStreaks.c_str()); }
	String^ Season::getsquad_fpp_weaponsAcquired() { return gcnew String(m_instance->s_squad_fpp_weaponsAcquired.c_str()); }
	String^ Season::getsquad_fpp_wins() { return gcnew String(m_instance->s_squad_fpp_wins.c_str()); }
	String^ Season::getsquad_fpp_top10s() { return gcnew String(m_instance->s_squad_fpp_top10s.c_str()); }
	String^ Season::getsquad_fpp_dailyKills() { return gcnew String(m_instance->s_squad_fpp_dailyKills.c_str()); }
	String^ Season::getsquad_fpp_weeklyKills() { return gcnew String(m_instance->s_squad_fpp_weeklyKills.c_str()); }
	String^ Season::getsquad_fpp_dailyWins() { return gcnew String(m_instance->s_squad_fpp_dailyWins.c_str()); }
	String^ Season::getsquad_fpp_weeklyWins() { return gcnew String(m_instance->s_squad_fpp_weeklyWins.c_str()); }
	String^ Season::getsquad_fpp_damageDealt() { return gcnew String(m_instance->s_squad_fpp_damageDealt.c_str()); }
	String^ Season::getsquad_fpp_headshotKills() { return gcnew String(m_instance->s_squad_fpp_headshotKills.c_str()); }
}