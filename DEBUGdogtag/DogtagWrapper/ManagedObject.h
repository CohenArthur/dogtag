#pragma once

using namespace System;

namespace DogtagCLI {
	template<class T>
	public ref class ManagedObject {
	protected:
		T *m_instance;
	public:
		ManagedObject (T *instance)
			: m_instance(instance) {
		}

		virtual ~ManagedObject() {
			if (m_instance != nullptr) { delete m_instance; }
		}

		!ManagedObject() {
			if (m_instance != nullptr) { delete m_instance; }
		}
		T *getInstance() { return m_instance; }
	};
}

using namespace System::Runtime::InteropServices;

static const char* stringToCharArray(String^ string)
{
	const char* str = (const char*)(Marshal::StringToHGlobalAnsi(string)).ToPointer();
	return str;
}