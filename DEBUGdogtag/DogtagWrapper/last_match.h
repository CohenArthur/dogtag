#pragma once

//WRAPPER CLASS

#include "ManagedObject.h"
#include "user.h"
#include "../DEBUGdogtag/dogtag.h"

namespace DogtagCLI {
	public ref class LastMatch : public ManagedObject<DogtagCore::LastMatch>
	{
	public:
		LastMatch(String^ last_match_id, String^ user_name);

		void setLastMatchInfo(int verbose);
		void setAllData();
		void displayAllData();

		String^ 	getrank();
		String^ 	getkills();
		String^		getdbnos();
		String^		getassists();
		String^ 	getkillRank();
		String^ 	getdamageDealt();
		String^ 	getheadshotKills();
		String^ 	getheals();
		String^ 	getboosts();
		String^ 	getlongestKill();
		String^ 	gettimeSurvived();
		String^ 	getrevives();
		String^ 	getrideDistance();
		String^ 	getroadKills();
		String^ 	getteamKills();
		String^ 	getvehicleDestroys();
		String^ 	getwalkDistance();
		String^ 	getweaponsAcquired();
	};
}