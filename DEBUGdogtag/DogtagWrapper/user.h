#pragma once

//WRAPPER CLASS

#include "ManagedObject.h"
#include "../DEBUGdogtag/dogtag.h"

#include <string>

namespace DogtagCLI {
	public ref class User : public ManagedObject<DogtagCore::User> {
	public:
		User();
		User(String^ name);

		void	setUserData(int verbose);
		String^ getName();
		String^ getId();
		String^ getLastMatchId();
		String^ getCurrentSeasonId();
	private:
		String^ u_name;
		String^ u_id;
		String^ u_current_season_id;
	};
}