#include "user.h"

//WRAPPER CLASS

#include <string>
#include <iostream>

using namespace System;

namespace DogtagCLI {
	User::User()
		: ManagedObject(new DogtagCore::User()) {
	
	}

	User::User(String^ name)
		: ManagedObject(new DogtagCore::User(stringToCharArray(name))) {
	
	}

	void User::setUserData(int verbose) {
		try {
			m_instance->setUserData(verbose);
		}
		catch (Exception^ ex) {
			throw gcnew System::Exception(gcnew String("Setting Data for User failed, probably due to too many API Calls. Please try again in a bit !"));
		}
	}

	String^ User::getName() {
		return gcnew String(m_instance->getName().c_str());
	}

	String^ User::getId() {
		return gcnew String(m_instance->getId().c_str());
	}

	String^ User::getLastMatchId() {
		return gcnew String(m_instance->getLastMatchId().c_str());
	}

	String^ User::getCurrentSeasonId() {
		return gcnew String(m_instance->getCurrentSeasonId().c_str());
	}
}