#include "last_match.h"

//WRAPPER CLASS

using namespace System;

namespace DogtagCLI {
	LastMatch::LastMatch(String^ last_match_id, String^ user_name)
		: ManagedObject(new DogtagCore::LastMatch(stringToCharArray(last_match_id), stringToCharArray(user_name))) {

	}

	void LastMatch::setLastMatchInfo(int verbose) {
		try {
			m_instance->setLastMatchInfo(verbose);
		}
		catch (Exception^ ex) {
			throw gcnew System::Exception(gcnew String("Fetching Data for LastMatch failed, probably due to too many API Calls. Please try again in a bit !"));
		}
	}

	void LastMatch::setAllData() {

		try { 
			m_instance->setAllData(); 
		}
		catch (Exception^ ex) {
			throw gcnew System::Exception(gcnew String("Setting Data for LastMatch failed, probably due to too many API Calls. Please try again in a bit !"));
		}
	}

	void LastMatch::displayAllData() {
		m_instance->displayAllData();
	}

	String^ 	LastMatch::getrank() { return gcnew String(m_instance->lm_rank.c_str()); }
	String^ 	LastMatch::getkills() { return gcnew String(m_instance->lm_kills.c_str()); }
	String^		LastMatch::getdbnos() { return gcnew String(m_instance->lm_dbnos.c_str()); }
	String^		LastMatch::getassists() { return gcnew String(m_instance->lm_assists.c_str()); }
	String^ 	LastMatch::getkillRank() { return gcnew String(m_instance->lm_killRank.c_str()); }
	String^ 	LastMatch::getdamageDealt() { return gcnew String(m_instance->lm_damageDealt.c_str()); }
	String^ 	LastMatch::getheadshotKills() { return gcnew String(m_instance->lm_headshotKills.c_str()); }
	String^ 	LastMatch::getheals() { return gcnew String(m_instance->lm_heals.c_str()); }
	String^ 	LastMatch::getboosts() { return gcnew String(m_instance->lm_boosts.c_str()); }
	String^ 	LastMatch::getlongestKill() { return gcnew String(m_instance->lm_longestKill.c_str()); }
	String^ 	LastMatch::gettimeSurvived() { return gcnew String(m_instance->lm_timeSurvived.c_str()); }
	String^ 	LastMatch::getrevives() { return gcnew String(m_instance->lm_revives.c_str()); }
	String^ 	LastMatch::getrideDistance() { return gcnew String(m_instance->lm_rideDistance.c_str()); }
	String^ 	LastMatch::getroadKills() { return gcnew String(m_instance->lm_roadKills.c_str()); }
	String^ 	LastMatch::getteamKills() { return gcnew String(m_instance->lm_teamKills.c_str()); }
	String^ 	LastMatch::getvehicleDestroys() { return gcnew String(m_instance->lm_vehicleDestroys.c_str()); }
	String^ 	LastMatch::getwalkDistance() { return gcnew String(m_instance->lm_walkDistance.c_str()); }
	String^ 	LastMatch::getweaponsAcquired() { return gcnew String(m_instance->lm_weaponsAcquired.c_str()); }
}