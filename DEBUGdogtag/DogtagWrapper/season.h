#pragma once		

//WRAPPER CLASS

#include "ManagedObject.h"
#include "../DEBUGdogtag/dogtag.h"

namespace DogtagCLI {
	public ref class Season : public ManagedObject<DogtagCore::Season> {
	public:
		Season();
		Season(String^ id, int timeline);

		void setAllSeasonsData(int verbose);
		void setUserSeasonData(int verbose, int timeline);
		void displayAllStats();

		//SEASON_STATS
		////duo_TPP
		String^ getduo_tpp_kills();
		String^ getduo_tpp_dBNOs();
		String^ getduo_tpp_assists();
		String^ getduo_tpp_heals();
		String^ getduo_tpp_boosts();
		String^ getduo_tpp_revives();
		String^ getduo_tpp_longestKill();
		String^ getduo_tpp_roundMostKills();
		String^ getduo_tpp_teamKills();
		String^ getduo_tpp_maxKillStreaks();
		String^ getduo_tpp_weaponsAcquired();
		String^ getduo_tpp_wins();
		String^ getduo_tpp_top10s();
		String^ getduo_tpp_dailyKills();
		String^ getduo_tpp_weeklyKills();
		String^ getduo_tpp_dailyWins();
		String^ getduo_tpp_weeklyWins();
		String^ getduo_tpp_damageDealt();
		String^ getduo_tpp_headshotKills();
		////duo_FPP
		String^ getduo_fpp_kills();
		String^ getduo_fpp_dBNOs();
		String^ getduo_fpp_assists();
		String^ getduo_fpp_heals();
		String^ getduo_fpp_boosts();
		String^ getduo_fpp_revives();
		String^ getduo_fpp_longestKill();
		String^ getduo_fpp_roundMostKills();
		String^ getduo_fpp_teamKills();
		String^ getduo_fpp_maxKillStreaks();
		String^ getduo_fpp_weaponsAcquired();
		String^ getduo_fpp_wins();
		String^ getduo_fpp_top10s();
		String^ getduo_fpp_dailyKills();
		String^ getduo_fpp_weeklyKills();
		String^ getduo_fpp_dailyWins();
		String^ getduo_fpp_weeklyWins();
		String^ getduo_fpp_damageDealt();
		String^ getduo_fpp_headshotKills();
		////solo_TP
		String^ getsolo_tpp_kills();
		String^ getsolo_tpp_dBNOs();
		String^ getsolo_tpp_assists();
		String^ getsolo_tpp_heals();
		String^ getsolo_tpp_boosts();
		String^ getsolo_tpp_revives();
		String^ getsolo_tpp_longestKill();
		String^ getsolo_tpp_roundMostKills();
		String^ getsolo_tpp_teamKills();
		String^ getsolo_tpp_maxKillStreaks();
		String^ getsolo_tpp_weaponsAcquired();
		String^ getsolo_tpp_wins();
		String^ getsolo_tpp_top10s();
		String^ getsolo_tpp_dailyKills();
		String^ getsolo_tpp_weeklyKills();
		String^ getsolo_tpp_dailyWins();
		String^ getsolo_tpp_weeklyWins();
		String^ getsolo_tpp_damageDealt();
		String^ getsolo_tpp_headshotKills();
		////solo_FP
		String^ getsolo_fpp_kills();
		String^ getsolo_fpp_dBNOs();
		String^ getsolo_fpp_assists();
		String^ getsolo_fpp_heals();
		String^ getsolo_fpp_boosts();
		String^ getsolo_fpp_revives();
		String^ getsolo_fpp_longestKill();
		String^ getsolo_fpp_roundMostKills();
		String^ getsolo_fpp_teamKills();
		String^ getsolo_fpp_maxKillStreaks();
		String^ getsolo_fpp_weaponsAcquired();
		String^ getsolo_fpp_wins();
		String^ getsolo_fpp_top10s();
		String^ getsolo_fpp_dailyKills();
		String^ getsolo_fpp_weeklyKills();
		String^ getsolo_fpp_dailyWins();
		String^ getsolo_fpp_weeklyWins();
		String^ getsolo_fpp_damageDealt();
		String^ getsolo_fpp_headshotKills();
		////squad_TP
		String^ getsquad_tpp_kills();
		String^ getsquad_tpp_dBNOs();
		String^ getsquad_tpp_assists();
		String^ getsquad_tpp_heals();
		String^ getsquad_tpp_boosts();
		String^ getsquad_tpp_revives();
		String^ getsquad_tpp_longestKill();
		String^ getsquad_tpp_roundMostKills();
		String^ getsquad_tpp_teamKills();
		String^ getsquad_tpp_maxKillStreaks();
		String^ getsquad_tpp_weaponsAcquired();
		String^ getsquad_tpp_wins();
		String^ getsquad_tpp_top10s();
		String^ getsquad_tpp_dailyKills();
		String^ getsquad_tpp_weeklyKills();
		String^ getsquad_tpp_dailyWins();
		String^ getsquad_tpp_weeklyWins();
		String^ getsquad_tpp_damageDealt();
		String^ getsquad_tpp_headshotKills();
		////squad_FP
		String^ getsquad_fpp_kills();
		String^ getsquad_fpp_dBNOs();
		String^ getsquad_fpp_assists();
		String^ getsquad_fpp_heals();
		String^ getsquad_fpp_boosts();
		String^ getsquad_fpp_revives();
		String^ getsquad_fpp_longestKill();
		String^ getsquad_fpp_roundMostKills();
		String^ getsquad_fpp_teamKills();
		String^ getsquad_fpp_maxKillStreaks();
		String^ getsquad_fpp_weaponsAcquired();
		String^ getsquad_fpp_wins();
		String^ getsquad_fpp_top10s();
		String^ getsquad_fpp_dailyKills();
		String^ getsquad_fpp_weeklyKills();
		String^ getsquad_fpp_dailyWins();
		String^ getsquad_fpp_weeklyWins();
		String^ getsquad_fpp_damageDealt();
		String^ getsquad_fpp_headshotKills();
		//SEASON_STATS
	};
}