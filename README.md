DogTag PUBG Helper
==================

The Goal
--------

With this application, we intend to provide an easy to use, fast, and pretty
desktop application, that will teach new players complex concepts of PUBG while
also providing a fast access to statistics or information for advanced players.

Requirements
------------

- You absolutely NEED to have Visual Studio Community with the 'Desktop Development with C++' module installed. Sorry for the hacky workaround, but we couldn't resolve the dependency issue in time for the first submission. The total size of the download is 3.31 GB. Sorry again. If Logging in does not work and throws a .dll exception, it's probably because you don't have the module installed.

What is currently implemented
-----------------------------

- Logging in with your PUBG username
- No need to relog afterwards, unless you want to
- Checking out your stats
    - Switching between FPP/TPP, Solo/Duo/Squad
    - No loading time when switching between these modes, contrary to the PUBG Game
    - Very fast loading of new information
- Fast Map Checking
- Average shots to kill on different helmets and armors
- Character representation when wearing different helmets/armors

What we have to fix
-------------------

- Memory leaks when opening a new window.
    - We simply did not have time to fix them, but they are acknowledged. They are a priority, but we wanted to have more functionnality for the first submission.
- Linking the Weapons/Grip/Muzzle database to the buttons on the UI
    - Again, not enough time. It's almost implemented on our test builds.
- Linking amount of Shots to Kill to the selected weapon
- Optimizing the number of API Calls
    - As you know, we are limited to 10 API calls per minute when using the PUBG API. Which is why we need to implement different optimizations, and in particular a shared Queue between all users.

Keep in Mind
------------

- We are EXTREMELY limited by the PUBG API regarding the amount of requests we can make per minute. It is currently at a ratio of 10 requests/minute, which is abismally low considering checking out your stats requires at least 3 API requests. Optimizing the number of API Calls is a priority, but please be mindful of the harsh limit that is imposed on us.

Thanks !
========

The Dogtag Team
